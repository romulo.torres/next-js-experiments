export default function ListUsuario(req, res) {
  return res.status(200).json({
    data: [
      { id: 1, nome: "Rômulo Torres" },
      { id: 2, nome: "Rômulo Gadelha" },
      { id: 3, nome: "Bruno Sousa" },
      { id: 4, nome: "Cesar Filho" },
    ],
    count: 4,
  });
}
