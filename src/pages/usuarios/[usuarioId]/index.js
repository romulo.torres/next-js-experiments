import { useRouter } from "next/router";

export const getServerSideProps = async (ctx) => {
  console.log("Isso roda no Servidor");
  return {
    props: {
      data: [
        { id: 1, nome: "Rômulo Torres" },
        { id: 2, nome: "Rômulo Gadelha" },
        { id: 3, nome: "Bruno Sousa" },
        { id: 4, nome: "Cesar Filho" },
      ],
    },
  };
};

export default function PerfilUsuario(props) {
  const router = useRouter();

  const { usuarioId } = router.query;
  console.log("Isso roda no Cliente");
  console.log(props);

  return <div>Perfil do Usuário {usuarioId}</div>;
}
